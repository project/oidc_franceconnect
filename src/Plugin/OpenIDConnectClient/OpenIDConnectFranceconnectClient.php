<?php

namespace Drupal\oidc_franceconnect\Plugin\OpenIDConnectClient;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;

/**
 * FranceConnect OpenID Connect client.
 *
 * Implements OpenID Connect Client plugin for FranceConnect.
 *
 * @OpenIDConnectClient(
 *   id = "oidc_franceconnect",
 *   label = @Translation("FranceConnect")
 * )
 */
class OpenIDConnectFranceconnectClient extends OpenIDConnectClientBase {

  /**
   * A list of user info fields available in FranceConnect.
   *
   * @var array
   */
  protected static $userinfoFields = [

    'given_name' => 'Prénom',
    'family_name' => 'Nom de naissance',
    'birthdate' => 'Date de naissance',
    'idp_birthdate'  => 'Date de naissance (FD)',
    'gender' => 'Civilité',
    'birthcountry' => 'Code INSEE du pays de naissance',
    'birthplace' => 'Code INSEE du lieu de naissance',
    'email' => 'Courriel',
    'preferred_username' => 'Nom d\'usage',
    'address' => 'Adresse',
    'phone' => 'Téléphone',
    'identite_pivot' => 'Identité pivot',
    // dgfip_rfr
    // dgfip_nbpart
    // dgfip_sitfam
    // dgfip_pac
    // dgfip_aft
    // cnam_paiements_ij
    // connexion_tracks 
  ];

  /**
   * A list of fields we always request from the site.
   *
   * @var array
   */
  protected static $alwaysFetchFields = [
    'profile' => 'Profil',
    'email' => 'Courriel',
  ];

  /**
   * A mapping of userinfo fields to the scopes required to receive them.
   *
   * @var array
   */
  protected static $fieldToScopeMap = [ 
    'given_name' => 'given_name',
    'family_name' => 'family_name',
    'birthdate' => 'birthdate',
    'idp_birthdate' => 'idp_birthdate',
    'gender' => 'gender',
    'birthcountry' => 'birthcountry',
    'birthplace' =>  'birthplace',
    'email' => 'email',
    'preferred_username' => 'preferred_username',
    'address' => 'address',
    'phone' => 'phone',
    // dgfip_rfr
    // dgfip_nbpart
    // dgfip_sitfam
    // dgfip_pac
    // dgfip_aft
    // cnam_paiements_ij
    // connexion_tracks 

  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'acr_values' => ['eidas1'],
      'sandbox_mode' => TRUE,
      'userinfo_fields' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['sandbox_mode'] = [
      '#title' => $this->t('Intégration / test'),
      '#type' => 'checkbox',
      '#description' => $this->t('Cochez cette case si vous êtes en phase de test/intégration.'),
      '#default_value' => $this->configuration['sandbox_mode'],
    ];

    $form['acr_values'] = [
      '#title' => $this->t('Niveau eIDAS'),
      '#type' => 'select',
      '#options' => [
        'eidas1' => $this->t('eIDAS 1 - Faible'),
        'eidas2' => $this->t('eIDAS 2 - Substantiel'),
        'eidas3' => $this->t('eIDAS 3 - Fort'),
      ],
      '#default_value' => $this->configuration['acr_values'],
    ];


    $form['userinfo_fields'] = [
      '#title' => $this->t('User fields'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => static::$userinfoFields,
      '#description' => $this->t('Liste des champs à récupérer. Voir la @franceconnect_documentation pour plus de details. 
      Les champs email et profile sont toujours récupérés.', ['@franceconnect_documentation' => Link::fromTextAndUrl($this->t('documentation France Connect'), Url::fromUri('https://partenaires.franceconnect.gouv.fr/documentation'))->toString()]),
      '#default_value' => $this->configuration['userinfo_fields'],
    ];


    return $form;
  }



/**
* {@inheritdoc}
*/
  public function getEndpoints(): array {
    return $this->configuration['sandbox_mode'] ? [
      'authorization' => 'https://fcp.integ01.dev-franceconnect.fr/api/v1/authorize',
      'token' => 'https://fcp.integ01.dev-franceconnect.fr/api/v1/token',
      'userinfo' => 'https://fcp.integ01.dev-franceconnect.fr/api/v1/userinfo',
      'logout' => 'https://fcp.integ01.dev-franceconnect.fr/api/v1/logout',
    ] :
    [
      'authorization' => 'https://app.franceconnect.gouv.fr/api/v1/authorize',
      'token' => 'https://app.franceconnect.gouv.fr/api/v1/token',
      'userinfo' => 'https://app.franceconnect.gouv.fr/api/v1/userinfo',
      'logout' => 'https://app.franceconnect.gouv.fr/api/v1/logout',
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getClientScopes(): ?array {
    $fields = static::$alwaysFetchFields + ($this->configuration['userinfo_fields'] ?? []);
    return array_values(array_unique(array_intersect_key(static::$fieldToScopeMap, $fields)));
  }



  /**
   * Generate nonce (mandatory for FC).
   * 
   * As in the intergation demo, the nonce is a string starting with 'nonce' and followed by 64 characters, we do the same :/
   *
   * @param int $length
   *   The length of the nonce.
   *
   * @return string
   *   The nonce.
   */
  protected function generateNonce(int $length = 64): string {
    return 'nonce'.strtolower(substr(Crypt::randomBytesBase64($length), 0, $length));
  }



  /**
   * {@inheritdoc}
   */
  protected function getUrlOptions(string $scope, GeneratedUrl $redirect_uri): array {
    $options = parent::getUrlOptions($scope, $redirect_uri);

    $nonce = $this->generateNonce();
    $acr_values = $this->configuration['acr_values'];
    $options['query'] += [
      'nonce' => $nonce,
      'acr_values' => $acr_values
    ];
    $this->requestStack->getCurrentRequest()->getSession()->set('oidc_franceconnect.nonce', $nonce);

    return $options;
  }


}
