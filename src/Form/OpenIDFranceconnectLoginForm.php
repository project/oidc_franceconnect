<?php

namespace Drupal\oidc_franceconnect\Form;

use Drupal\openid_connect\Form\OpenIDConnectLoginForm;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\OpenIDConnectSessionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides the OpenID FranceConnect login form.
 *
 * @package Drupal\oidc_franceconnect\Form
 */
class OpenIDFranceconnectLoginForm extends OpenIDConnectLoginForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $clients = $this->entityTypeManager->getStorage('openid_connect_client')->loadByProperties(['status' => TRUE]);
    foreach ($clients as $client_id => $client) {
      /** TODO : get pluginId to ensure it is franceconnect type; test = dpm($client->getPlugin()); */
      /** @var \Drupal\openid_connect\OpenIDConnectClientEntityInterface $client */
      $form['openid_connect_client_' . $client_id . '_login'] = [
        '#type' => 'submit',
        '#value' => $this->t('Log in with @client_title', [
          '@client_title' => $client->label(),
        ]),
        '#name' => $client_id,
        '#prefix' => '<div id="fc-prefix"><p>FranceConnect est la solution proposée par l’État pour sécuriser et simplifier la connexion à vos services en ligne.</p></div>',
        '#suffix' => '<div id="fc-suffix"><p><a href="https://franceconnect.gouv.fr/" target="_blank">Qu’est-ce que FranceConnect ?</a></p></div>',
        '#attributes' => array('id' => array('btn-franceconnect')),
        '#attached' => array('library' => array('oidc_franceconnect/oidc-franceconnect-block')),
        
        ];
    }
    return $form;
  }


}
